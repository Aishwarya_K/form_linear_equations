from sympy import symbols

#2 variable equations
x,y,x1,y1 = symbols('x y x1 y1')
def form_eqn2(a,b):
    output=[]
    for m in range(1,11):
        eqn = (y-y1)- m*(x-x1)
        res= eqn.subs(x1,a).subs(y1,b)
        output.append(str(res)+ " = 0")
    return output

#3 variable equations
x,y,z,x1,y1,z1 = symbols('x y z x1 y1 z1')
def form_eqn3(a,b,c):
    output=[]
    for m1 in range(1,5):
        for m2 in range(1,5):
            eqn = (z-z1)- m1*(y-y1)- m2*(x-x1)
            res= eqn.subs(x1,a).subs(y1,b).subs(z1,c)
            output.append(str(res)+ " = 0")
    return output

#4 variable equations
w,x,y,z,w1,x1,y1,z1 = symbols('w x y z w1 x1 y1 z1')
def form_eqn4(a,b,c,d):
    output=[]
    for m1 in range(1,4):
        for m2 in range(1,3):
            for m3 in range(1,3):
                eqn = (z-z1)- m1*(y-y1)- m2*(x-x1)- m3*(w-w1)
                res= eqn.subs(w1,a).subs(x1,b).subs(y1,c).subs(z1,d)
                output.append(str(res)+ " = 0")
    return output

def form_eqn(l):
    if len(l)==2:
        return form_eqn2(l[0],l[1])
    elif len(l)==3:
        return form_eqn3(l[0],l[1],l[2])
    elif len(l)==4:
        return form_eqn4(l[0],l[1],l[2],l[3])


