from fastapi import FastAPI
import uvicorn
from pydantic import BaseModel
from sympy import symbols
from generate_eqn import *

app= FastAPI()

class input(BaseModel):
    solution : list
   

@app.post('/form_eqn/')
async def gen_eqns(item: input):
    l = form_eqn(item.solution)
    result=[]
    for i in range(len(l)):
        result.append(l[i].replace("*",""))
    return result
    

